/**
 @file helloWorldimpl.cpp

 @author Your-name-here <your.email.here@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */


#include "helloWorld.h"

#include<pluto/pins/integer.hpp>
#include<pluto/pins/string.hpp>
#include<pluto/pins/dio.hpp>
#include<pluto/pins/sync.hpp>


using namespace std;
using namespace Pluto;
using namespace Pluto::Pins;

helloWorld::helloWorld(std::string appName M_UU):
    MarelCppApp(appName)
{

    //TODO Implement your construction time initialization
}


helloWorld::~helloWorld()
{
    //TODO Implement your cleanup here
}

void helloWorld::onNewstateNumberA(IntegerSlot* slot, int64_t value, Tick t)
{
    std::cout << "Wohoo got value "<<value<<std::endl;
    numAvalue = value;
    _sum.set(numAvalue+numBvalue);

    cout << "sum is :" << _sum.getState() << endl;
}
void helloWorld::onNewstateNumberB(IntegerSlot* slot, int64_t value, Tick t)
{
    std::cout << "Wohoo got value "<<value<<std::endl;
    numBvalue = value;
    _sum.set(numAvalue+numBvalue);
    cout << "sum is :" << numAvalue + numBvalue << " and " << _sum.getState() << endl;
}

int32_t
helloWorld::initialize(int32_t argc M_UU, char** argv M_UU)
{
   cout << endl << __PRETTY_FUNCTION__ << endl;

   _numberA.setNewstateCallback(this, &helloWorld::onNewstateNumberA);
   _numberB.setNewstateCallback(this, &helloWorld::onNewstateNumberB);

   //TODO Implement your initialization functionality here
   _sum.registerPin("sum");
   _numberA.registerPin("numberA");
   _numberB.registerPin("numberB");
   _myDIOSignal.registerPin("dioSignal");
   _myDIOSlot.registerPin("dioSlot");
   _myStringSignal.registerPin("stringSignal");
   _myStringSlot.registerPin("stringSlot");
   _mySyncSignal.registerPin("syncSignal");
   _mySyncSlot.registerPin("syncSlot");

   return 0;
}


void
helloWorld::deinitialize()
{
    cout << endl << __PRETTY_FUNCTION__ << endl;

    //TODO Implement your deinitialization functionality here
}


void
helloWorld::timeout()
{
    cout << endl << __PRETTY_FUNCTION__ << endl;

    //TODO Implement your timeout functionality here
}


int32_t
helloWorld::onTmDead(mqs_streamid_t sid M_UU, const char* ep M_UU)
{
    cout << endl << __PRETTY_FUNCTION__ << endl;

    //TODO Implement your ticker master exit functionality here
    //Warning: Deprecated; will be come obsolete in near future release

    return 0;
}


int32_t
helloWorld::onTmAlive(mqs_streamid_t sid M_UU, const char *ep M_UU)
{
    cout << endl << __PRETTY_FUNCTION__ << endl;

    //TODO Implement your ticker master entry functionality here
    //Warning: Deprecated; will be come obsolete in near future release

    return 0;
}

