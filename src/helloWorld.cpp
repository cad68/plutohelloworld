/**
 @file helloWorld.cpp

 @author Your-name-here <your.email.here@marel.com>

 <b>
 Copyright (c) 2018 Marel hf
 Proprietary material, all rights reserved, use or disclosure forbidden.
 </b>
 */


#include <MarelCppApp.h>
#include "helloWorld.h"


int main(int argc, char** argv)
{
    helloWorld* marelApp;
    
    marelApp = new helloWorld(argv[0]);
    if(marelApp)
    {
        return marelApp->startUp(argc, argv);
    }

    return -1;
}

