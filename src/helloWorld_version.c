#include <stdlib.h>
#include "mversion.h"

static struct mversion _mversion_version = {
    .structversion = MVERSION,
    .projecttype = "PROG",
    .svnrev = 0,
    .localmodifications= 0,
    .compname = "helloWorld",
    .builder = "pluto",
    .machine = "pluto2",
    .url = "https://cad68@bitbucket.org/cad68/plutohelloworld.git",
    .date = "Thu Mar  1 07:03:15 UTC 2018",
    .major = 0,
    .minor = 0,
    .patch = 0,
    .buildnumber = 0,
    .extraversion = "",
    .gitsha1 = "",
};

static void __attribute__((constructor)) _versioninit( void )
{
    mversion_add( &_mversion_version );
}

static void __attribute__((destructor)) _versiondeinit( void )
{
    mversion_remove( &_mversion_version );
}

const char * helloWorld_svnversion( void )
{
	return "This function is no longer supported. Please use mversion_fprintf() in mversion library."
		"0.0.0-0";
}

const char * helloWorld_longsvnversion( void )
{
	return "This function is no longer supported. Please use mversion_fprintf() in mversion library."
		"helloWorld-0.0.0-0-pluto-Thu Mar  1 07:03:15 UTC 2018";
}
