/** @mainpage helloWorld Mainpage

  @section template Template Documentation
  This is the documentation for this project template. 
  @note <b>Please remove this section out of the Readme.txt when you 
  have created your own project from this template.</b>

  This is a C++ application with basic functions for initialization,
  deinitialization etc.
  - constructor: A normal constructor.
  - destructor: A normal destructor.
  -

  @subsection thecode
 
  This is the main page of your project. It is generated automatically from
  the dummy Readme that has been copied to your projects main path.
  You should change this file to get the documentation appropriate for
  your project

  @section desc Description
  Here a description of the library should follow.

  @section depend Dependencies
  Here the dependencies of the library have to be given, i.e. which library
  has to be loaded before.

  @section installstart Installation / Start
  Here you give informations about how to install the software package, i.e.
  which files have to go in which directory. Furthermore here should be
  said how the library has to be initialized

  @section hist History
  This section should give an overview of the changes made to the specific
  versions.
*/

/** This is a list of all example files used in this library.
 *  @example demo_example.c
 */

/** put a new example file in an extra comment block like the above
  */


/** if you have additional documentation (e.g. an OpenOffice/MSOffice doc)
    then export it as HTML and put one section per document similar to the
    following section.
    Do not forget to add another asterisk (*) so that doxygen is able to
    parse it.
  */

/* @page htmlinclude Additional documentation
    @htmlinclude anotherdoc.html
  */


