#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <appcbase.h>
#include <mqs_queue.h>
#include <gtest/gtest.h>

#include <iostream>
#include <fstream>

#include "helloWorld.h"


using namespace std;


/*
 * For a quick intro to Google test refer to: http://code.google.com/p/googletest/wiki/Primer
 *
 * The executable output of this source file is located in the test folder of the build folder.
 */

TEST(BasicTests, ClassConstruction)
{
	string name = "testname";
	helloWorld *pApp = new helloWorld(name);
	ASSERT_EQ(name, pApp->getAppName());
	ASSERT_TRUE(pApp != NULL);
}


int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

