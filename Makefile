##################################################
#                                                #
#            Marel Makefile                      #
#                                                #
##################################################

PRJ_NAME = helloWorld

PRJ_TYPE = PROG

MAIN_SRC = helloWorld.cpp

SRC = helloWorld_impl.cpp

TEST_SRC = gtest_helloWorld.cpp

TEST_REGR_PRGS =

ADD_CFLAGS = 

ADD_LFLAGS =

TEST_LDFLAGS = -lgtest

ADD_LIBS = appbase integerpin++ stringpin++ digitaliopin++ syncpin++
ADD_LIBS_PATH =

ADD_LINT_FLAGS=

default: debug

ifndef MDEV
    $(error Environment variable MDEV required)
endif

ifndef CPU
    $(error Environment variable CPU <x86/ppc> required)
endif


#
# include main makefile
#
include $(MDEV)/make/make.main

#eof
