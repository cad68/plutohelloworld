/**
 *   @defgroup helloWorld Put your doxygen label here
     @{

     @file helloWorld.h

     <REPLACE WITH YOUR BRIEF DESCRIPTION>
     A user application class that implements all the user defined functionality

     <REPLACE WITH YOUR DESCRIPTION>
     When a new project is being created, a class like this one is created
     (with relevant class/method name) which inherits 'MarelCppApp'. This class
     should then represent the implementation of the application creator


     @author your-name-here <your.email.here@marel.com>

     <b>
     Copyright (c) 2018 Marel hf
     Proprietary material, all rights reserved, use or disclosure forbidden.
     </b>
 */

#ifndef _helloWorld_H
#define _helloWorld_H


#define M_helloWorld_MODULEID           "helloWorld"
#ifndef LOGGER_MODULEID
#define LOGGER_MODULEID                     (M_helloWorld_MODULEID)
#endif


#include <MarelCppApp.h>

#include <integer_pin.h>
#include <digitalio_pin.h>
#include <string_pin.h>
#include<pluto/pins/integer.hpp>
#include<pluto/pins/string.hpp>
#include<pluto/pins/dio.hpp>
#include<pluto/pins/sync.hpp>


using namespace Pluto;
using namespace Pluto::Pins;

class helloWorld: public MarelCppApp
{
    public:
        /*
         * Constructor
         */
        helloWorld(std::string appName M_UU);

        /*
         * Destructor
         */
        ~helloWorld();


        /**
         * The timeout function is periodically called by the application
         * loop. The length of the period is defined by the implementer by
         * calling 'setTimeout'
         *
         * @see setTimeout
         */
        void timeout();

        /**
         * Initializes the implementation class
         *
         * @param argc Number of arguments
         * @param argv Array of arguments
         * @return 0 if successful else -1
         */
        int32_t initialize(int32_t argc M_UU, char** argv M_UU);

        /**
         * Deinitializes the implementation class
         */
        void deinitialize();

        /**
         * Called when ticker master deceases
         *
         * DEPRECATED
         */
        int32_t onTmDead(mqs_streamid_t sid M_UU, const char* ep M_UU);

        /**
         * Called when ticker master is up and running
         *
         * DEPRECATED 
         */
        int32_t onTmAlive(mqs_streamid_t sid M_UU, const char* ep M_UU);

	void onNewstateNumberA(IntegerSlot* slot, int64_t value, Tick t);
	void onNewstateNumberB(IntegerSlot* slot, int64_t value, Tick t);

    private: 
        IntegerSignal _sum;
        IntegerSlot   _numberA;
        IntegerSlot   _numberB;
        DIOSignal     _myDIOSignal;
        DIOSlot       _myDIOSlot;
        StringSignal  _myStringSignal;
        StringSlot    _myStringSlot;
        SyncSignal    _mySyncSignal;
        SyncSlot      _mySyncSlot;

	int64_t numAvalue;
	int64_t numBvalue;
        int64_t sum;
};

/**@}*/


#endif /* helloWorld_H_ */

