angular.module('GuiApp')
.controller('Summer', ['PlutoVariables', 'widgetService', function(PlutoVariables, widgetService) {

    var ctrl = this;

    ctrl.numberA = 50;
    ctrl.numAChanged = function(val) {
        // implement here what to do when the value is changed in the spinner
        PlutoVariables.publish("integer.wsserver.0.numA",val);
        console.log("Var A " +  val);
        // in this example, just change status bar:
        widgetService.StatusBar.global.setStatusMessage("A changed to: '" + val + "'");
    };

    ctrl.numberB = 2;
    ctrl.numBChanged = function(val) {
        // implement here what to do when the value is changed in the spinner

        console.log("Var B " +  val);
        PlutoVariables.publish("integer.wsserver.0.numB",val);
        // in this example, just change status bar:
        widgetService.StatusBar.global.setStatusMessage("B changed to: '" + val + "'");
    };
}]);
